/**
 * BCS
 * SOULATRE Manuel
 * 
 * SHA-2 implementation
 *
 */

#include "sha2.h"

#define ROTR32(x, y) (((x) >> (y)) ^ ((x) << (32 - (y))))
#define ROTR64(x, y) (((x) >> (y)) ^ ((x) << (64 - (y))))

#define SHA2_ROUNDS_256 64
#define SHA2_ROUNDS_512 80

static const uint_fast32_t k32[64] = {
  0x428a2f98u, 0x71374491u, 0xb5c0fbcfu, 0xe9b5dba5u, 0x3956c25bu, 0x59f111f1u, 0x923f82a4u, 0xab1c5ed5u,
  0xd807aa98u, 0x12835b01u, 0x243185beu, 0x550c7dc3u, 0x72be5d74u, 0x80deb1feu, 0x9bdc06a7u, 0xc19bf174u,
  0xe49b69c1u, 0xefbe4786u, 0x0fc19dc6u, 0x240ca1ccu, 0x2de92c6fu, 0x4a7484aau, 0x5cb0a9dcu, 0x76f988dau,
  0x983e5152u, 0xa831c66du, 0xb00327c8u, 0xbf597fc7u, 0xc6e00bf3u, 0xd5a79147u, 0x06ca6351u, 0x14292967u,
  0x27b70a85u, 0x2e1b2138u, 0x4d2c6dfcu, 0x53380d13u, 0x650a7354u, 0x766a0abbu, 0x81c2c92eu, 0x92722c85u,
  0xa2bfe8a1u, 0xa81a664bu, 0xc24b8b70u, 0xc76c51a3u, 0xd192e819u, 0xd6990624u, 0xf40e3585u, 0x106aa070u,
  0x19a4c116u, 0x1e376c08u, 0x2748774cu, 0x34b0bcb5u, 0x391c0cb3u, 0x4ed8aa4au, 0x5b9cca4fu, 0x682e6ff3u,
  0x748f82eeu, 0x78a5636fu, 0x84c87814u, 0x8cc70208u, 0x90befffau, 0xa4506cebu, 0xbef9a3f7u, 0xc67178f2u
};

static const uint_fast64_t k64[80] = {
  0x428a2f98d728ae22ull, 0x7137449123ef65cdull, 0xb5c0fbcfec4d3b2full, 0xe9b5dba58189dbbcull,
  0x3956c25bf348b538ull, 0x59f111f1b605d019ull, 0x923f82a4af194f9bull, 0xab1c5ed5da6d8118ull,
  0xd807aa98a3030242ull, 0x12835b0145706fbeull, 0x243185be4ee4b28cull, 0x550c7dc3d5ffb4e2ull,
  0x72be5d74f27b896full, 0x80deb1fe3b1696b1ull, 0x9bdc06a725c71235ull, 0xc19bf174cf692694ull,
  0xe49b69c19ef14ad2ull, 0xefbe4786384f25e3ull, 0x0fc19dc68b8cd5b5ull, 0x240ca1cc77ac9c65ull,
  0x2de92c6f592b0275ull, 0x4a7484aa6ea6e483ull, 0x5cb0a9dcbd41fbd4ull, 0x76f988da831153b5ull,
  0x983e5152ee66dfabull, 0xa831c66d2db43210ull, 0xb00327c898fb213full, 0xbf597fc7beef0ee4ull,
  0xc6e00bf33da88fc2ull, 0xd5a79147930aa725ull, 0x06ca6351e003826full, 0x142929670a0e6e70ull,
  0x27b70a8546d22ffcull, 0x2e1b21385c26c926ull, 0x4d2c6dfc5ac42aedull, 0x53380d139d95b3dfull,
  0x650a73548baf63deull, 0x766a0abb3c77b2a8ull, 0x81c2c92e47edaee6ull, 0x92722c851482353bull,
  0xa2bfe8a14cf10364ull, 0xa81a664bbc423001ull, 0xc24b8b70d0f89791ull, 0xc76c51a30654be30ull,
  0xd192e819d6ef5218ull, 0xd69906245565a910ull, 0xf40e35855771202aull, 0x106aa07032bbd1b8ull,
  0x19a4c116b8d2d0c8ull, 0x1e376c085141ab53ull, 0x2748774cdf8eeb99ull, 0x34b0bcb5e19b48a8ull,
  0x391c0cb3c5c95a63ull, 0x4ed8aa4ae3418acbull, 0x5b9cca4f7763e373ull, 0x682e6ff3d6b2b8a3ull,
  0x748f82ee5defb2fcull, 0x78a5636f43172f60ull, 0x84c87814a1f0ab72ull, 0x8cc702081a6439ecull,
  0x90befffa23631e28ull, 0xa4506cebde82bde9ull, 0xbef9a3f7b2c67915ull, 0xc67178f2e372532bull,
  0xca273eceea26619cull, 0xd186b8c721c0c207ull, 0xeada7dd6cde0eb1eull, 0xf57d4f7fee6ed178ull,
  0x06f067aa72176fbaull, 0x0a637dc5a2c898a6ull, 0x113f9804bef90daeull, 0x1b710b35131c471bull,
  0x28db77f523047d84ull, 0x32caab7b40c72493ull, 0x3c9ebe0a15c9bebcull, 0x431d67c49c100d4cull,
  0x4cc5d4becb3e42b6ull, 0x597f299cfc657e2aull, 0x5fcb6fab3ad6faecull, 0x6c44198c4a475817ul
};

void sha256_process(SHA256_CTX* ctx) {
  uint32_t w[SHA2_ROUNDS_256];
  uint32_t t[8];
  int_fast32_t i,x,y;

  for(i=0; i < 16; i++) {
    w[i] = (ctx->buffer[i*4] << 24) ^ (ctx->buffer[i*4 + 1] << 16) ^ (ctx->buffer[i*4 + 2] << 8) ^ (ctx->buffer[i*4 + 3]);
  }
  for( ; i < SHA2_ROUNDS_256; i++) {
    x = ROTR32(w[i-15],7) ^ ROTR32(w[i-15],18) ^ (w[i-15] >>  3);
    y = ROTR32(w[i-2],17) ^ ROTR32(w[i-2] ,19) ^ (w[i-2]  >> 10);
    w[i] = w[i-16] + x + w[i-7] + y;
  }

  for(i=0; i < 8; i++) {
    t[i] = ctx->state[i];
  }
  for(i=0; i < SHA2_ROUNDS_256; i++) {
    x = (ROTR32(t[4], 6) ^ ROTR32(t[4], 11) ^ ROTR32(t[4], 25)) + ((t[4] & t[5]) ^ ((~t[4]) & t[6])) + t[7] + k32[i] + w[i];
    y = (ROTR32(t[0], 2) ^ ROTR32(t[0], 13) ^ ROTR32(t[0], 22)) + ((t[0] & t[1]) ^ (t[0] & t[2]) ^ (t[1] & t[2]));
    t[7] = t[6];
    t[6] = t[5];
    t[5] = t[4];
    t[4] = t[3] + x;
    t[3] = t[2];
    t[2] = t[1];
    t[1] = t[0];
    t[0] = x + y;
  }
  for(i=0; i < 8; i++) {
    ctx->state[i] += t[i];
  }
}

void sha512_process(SHA512_CTX* ctx) {
  uint64_t w[SHA2_ROUNDS_512];
  uint64_t t[8];
  int_fast32_t i;
  uint64_t x,y;

  for(i=0; i < 16; i++) {
    w[i] = ((uint64_t)ctx->buffer[i*8]     << 56) ^ ((uint64_t)ctx->buffer[i*8 + 1] << 48)
        ^  ((uint64_t)ctx->buffer[i*8 + 2] << 40) ^ ((uint64_t)ctx->buffer[i*8 + 3] << 32)
        ^  ((uint64_t)ctx->buffer[i*8 + 4] << 24) ^ ((uint64_t)ctx->buffer[i*8 + 5] << 16)
        ^  ((uint64_t)ctx->buffer[i*8 + 6] << 8)  ^ ((uint64_t)ctx->buffer[i*8 + 7]);
  }
  for( ; i < SHA2_ROUNDS_512; i++) {
    x = ROTR64(w[i-15],1) ^ ROTR64(w[i-15],8) ^ (w[i-15] >> 7);
    y = ROTR64(w[i-2],19) ^ ROTR64(w[i-2],61) ^ (w[i-2]  >> 6);
    w[i] = w[i-16] + x + w[i-7] + y;
  }

  for(i=0; i < 8; i++) {
    t[i] = ctx->state[i];
  }
  for(i=0; i < SHA2_ROUNDS_512; i++) {
    x = (ROTR64(t[4], 14) ^ ROTR64(t[4], 18) ^ ROTR64(t[4], 41)) + ((t[4] & t[5]) ^ ((~t[4]) & t[6])) + t[7] + k64[i] + w[i];
    y = (ROTR64(t[0], 28) ^ ROTR64(t[0], 34) ^ ROTR64(t[0], 39)) + ((t[0] & t[1]) ^ (t[0] & t[2]) ^ (t[1] & t[2]));
    t[7] = t[6];
    t[6] = t[5];
    t[5] = t[4];
    t[4] = t[3] + x;
    t[3] = t[2];
    t[2] = t[1];
    t[1] = t[0];
    t[0] = x + y;
  }
  for(i=0; i < 8; i++) {
    ctx->state[i] += t[i];
  }
}

void sha256_init(SHA256_CTX* ctx) {
  ctx->state[0] = 0x6a09e667u;
  ctx->state[1] = 0xbb67ae85u;
  ctx->state[2] = 0x3c6ef372u;
  ctx->state[3] = 0xa54ff53au;
  ctx->state[4] = 0x510e527fu;
  ctx->state[5] = 0x9b05688cu;
  ctx->state[6] = 0x1f83d9abu;
  ctx->state[7] = 0x5be0cd19u;

  ctx->bufferLength = 0;
  ctx->totalLength = 0;
  ctx->outputSize = SHA2_OUTPUT_SIZE_256;
}

void sha224_init(SHA256_CTX* ctx) {
  ctx->state[0] = 0xc1059ed8u;
  ctx->state[1] = 0x367cd507u;
  ctx->state[2] = 0x3070dd17u;
  ctx->state[3] = 0xf70e5939u;
  ctx->state[4] = 0xffc00b31u;
  ctx->state[5] = 0x68581511u;
  ctx->state[6] = 0x64f98fa7u;
  ctx->state[7] = 0xbefa4fa4u;

  ctx->bufferLength = 0;
  ctx->totalLength = 0;
  ctx->outputSize = SHA2_OUTPUT_SIZE_224;
}

void sha384_init(SHA512_CTX* ctx) {
  ctx->state[0] = 0xcbbb9d5dc1059ed8ull;
  ctx->state[1] = 0x629a292a367cd507ull;
  ctx->state[2] = 0x9159015a3070dd17ull;
  ctx->state[3] = 0x152fecd8f70e5939ull;
  ctx->state[4] = 0x67332667ffc00b31ull;
  ctx->state[5] = 0x8eb44a8768581511ull;
  ctx->state[6] = 0xdb0c2e0d64f98fa7ull;
  ctx->state[7] = 0x47b5481dbefa4fa4ull;

  ctx->bufferLength = 0;
  ctx->totalLength[0] = 0;
  ctx->totalLength[1] = 0;
  ctx->outputSize = SHA2_OUTPUT_SIZE_384;
}

void sha512_init(SHA512_CTX* ctx) {
  ctx->state[0] = 0x6a09e667f3bcc908ull;
  ctx->state[1] = 0xbb67ae8584caa73bull;
  ctx->state[2] = 0x3c6ef372fe94f82bull;
  ctx->state[3] = 0xa54ff53a5f1d36f1ull;
  ctx->state[4] = 0x510e527fade682d1ull;
  ctx->state[5] = 0x9b05688c2b3e6c1full;
  ctx->state[6] = 0x1f83d9abfb41bd6bull;
  ctx->state[7] = 0x5be0cd19137e2179ull;

  ctx->bufferLength = 0;
  ctx->totalLength[0] = 0;
  ctx->totalLength[1] = 0;
  ctx->outputSize = SHA2_OUTPUT_SIZE_512;
}

void sha256_update(SHA256_CTX* ctx, void* in, int inputSize) {
  int_fast32_t i;
  uint8_t* input = in;

  for(i = 0; i < inputSize; i++) {
    ctx->buffer[ctx->bufferLength++] = input[i];
    if(ctx->bufferLength == SHA2_CHUNK_SIZE_256) {
      // process whole chunk
      sha256_process(ctx);
      ctx->bufferLength = 0;
    }
  }
  ctx->totalLength += inputSize * 8;
}

void sha512_update(SHA512_CTX* ctx, void* in, int inputSize) {
  int_fast32_t i;
  uint64_t overflowDetection;
  uint8_t* input = in;

  for(i = 0; i < inputSize; i++) {
    ctx->buffer[ctx->bufferLength++] = input[i];
    if(ctx->bufferLength == SHA2_CHUNK_SIZE_512) {
      // process whole chunk
      sha512_process(ctx);
      ctx->bufferLength = 0;
    }
  }
  overflowDetection = ctx->totalLength[0];
  ctx->totalLength[0] += inputSize * 8;
  if(ctx->totalLength[0] < overflowDetection) {
    ctx->totalLength[1]++;
  }
}

void sha256_final(SHA256_CTX* ctx, void* out) {
  int_fast32_t i, j, imax;
  uint8_t* output = out;

  // pad last block
  ctx->buffer[ctx->bufferLength++] = 0x80;
  for(i=ctx->bufferLength; i < SHA2_CHUNK_SIZE_256; i++) {
    ctx->buffer[i] = 0;
  }
  if(ctx->bufferLength > SHA2_CHUNK_SIZE_256 - 8) {
    // process whole chunk in case there is not enough space to add total size (we need 64 bits)
    sha256_process(ctx);
    ctx->bufferLength = 0;

    for(i=0; i < SHA2_CHUNK_SIZE_256 - 8; i++) {
      ctx->buffer[i] = 0;
    }
  }

  ctx->buffer[SHA2_CHUNK_SIZE_256-1] = (ctx->totalLength)       & 0xff;
  ctx->buffer[SHA2_CHUNK_SIZE_256-2] = (ctx->totalLength >> 8)  & 0xff;
  ctx->buffer[SHA2_CHUNK_SIZE_256-3] = (ctx->totalLength >> 16) & 0xff;
  ctx->buffer[SHA2_CHUNK_SIZE_256-4] = (ctx->totalLength >> 24) & 0xff;
  ctx->buffer[SHA2_CHUNK_SIZE_256-5] = (ctx->totalLength >> 32) & 0xff;
  ctx->buffer[SHA2_CHUNK_SIZE_256-6] = (ctx->totalLength >> 40) & 0xff;
  ctx->buffer[SHA2_CHUNK_SIZE_256-7] = (ctx->totalLength >> 48) & 0xff;
  ctx->buffer[SHA2_CHUNK_SIZE_256-8] = (ctx->totalLength >> 56) & 0xff;

  sha256_process(ctx);

  imax = (ctx->outputSize == SHA2_OUTPUT_SIZE_256)? 8 : 7;
  for(i=0; i<imax; i++) {
    for(j=0; j<4; j++) {
      // endianess inversion for output
      output[i*4 + j] = (ctx->state[i] >> (24-j*8)) & 0xff;
    }
  }

  if(ctx->outputSize == SHA2_OUTPUT_SIZE_256) {
    sha256_init(ctx);
  } else {
    sha224_init(ctx);
  }
}

void sha512_final(SHA512_CTX* ctx, void* out) {
  int_fast32_t i, j, imax;
  uint8_t* output = out;

  // pad last block
  ctx->buffer[ctx->bufferLength++] = 0x80;
  for(i=ctx->bufferLength; i < SHA2_CHUNK_SIZE_512; i++) {
    ctx->buffer[i] = 0;
  }
  if(ctx->bufferLength > SHA2_CHUNK_SIZE_512 - 16) {
    // process whole chunk in case there is not enough space to add total size (we need 128 bits)
    sha512_process(ctx);
    ctx->bufferLength = 0;

    for(i=0; i < SHA2_CHUNK_SIZE_512 - 16; i++) {
      ctx->buffer[i] = 0;
    }
  }

  ctx->buffer[SHA2_CHUNK_SIZE_512-1]  = (ctx->totalLength[0])       & 0xff;
  ctx->buffer[SHA2_CHUNK_SIZE_512-2]  = (ctx->totalLength[0] >> 8)  & 0xff;
  ctx->buffer[SHA2_CHUNK_SIZE_512-3]  = (ctx->totalLength[0] >> 16) & 0xff;
  ctx->buffer[SHA2_CHUNK_SIZE_512-4]  = (ctx->totalLength[0] >> 24) & 0xff;
  ctx->buffer[SHA2_CHUNK_SIZE_512-5]  = (ctx->totalLength[0] >> 32) & 0xff;
  ctx->buffer[SHA2_CHUNK_SIZE_512-6]  = (ctx->totalLength[0] >> 40) & 0xff;
  ctx->buffer[SHA2_CHUNK_SIZE_512-7]  = (ctx->totalLength[0] >> 48) & 0xff;
  ctx->buffer[SHA2_CHUNK_SIZE_512-8]  = (ctx->totalLength[0] >> 56) & 0xff;
  ctx->buffer[SHA2_CHUNK_SIZE_512-9]  = (ctx->totalLength[1])       & 0xff;
  ctx->buffer[SHA2_CHUNK_SIZE_512-10] = (ctx->totalLength[1] >> 8)  & 0xff;
  ctx->buffer[SHA2_CHUNK_SIZE_512-11] = (ctx->totalLength[1] >> 16) & 0xff;
  ctx->buffer[SHA2_CHUNK_SIZE_512-12] = (ctx->totalLength[1] >> 24) & 0xff;
  ctx->buffer[SHA2_CHUNK_SIZE_512-13] = (ctx->totalLength[1] >> 32) & 0xff;
  ctx->buffer[SHA2_CHUNK_SIZE_512-14] = (ctx->totalLength[1] >> 40) & 0xff;
  ctx->buffer[SHA2_CHUNK_SIZE_512-15] = (ctx->totalLength[1] >> 48) & 0xff;
  ctx->buffer[SHA2_CHUNK_SIZE_512-16] = (ctx->totalLength[1] >> 56) & 0xff;

  sha512_process(ctx);

  imax = (ctx->outputSize == SHA2_OUTPUT_SIZE_512)? 8 : 6;
  for(i=0; i<imax; i++) {
    for(j=0; j<8; j++) {
      // endianess inversion for output
      output[i*8 + j] = (ctx->state[i] >> (56-j*8)) & 0xff;
    }
  }

  if(ctx->outputSize == SHA2_OUTPUT_SIZE_512) {
    sha512_init(ctx);
  } else {
    sha384_init(ctx);
  }
}

void sha256_do(void* in, int inputSize, void* out) {
  SHA256_CTX ctx;
  sha256_init(&ctx);
  sha256_update(&ctx, in, inputSize);
  sha256_final(&ctx, out);
}

void sha224_do(void* in, int inputSize, void* out) {
  SHA256_CTX ctx;
  sha224_init(&ctx);
  sha256_update(&ctx, in, inputSize);
  sha256_final(&ctx, out);
}

void sha384_do(void* in, int inputSize, void* out) {
  SHA512_CTX ctx;
  sha384_init(&ctx);
  sha512_update(&ctx, in, inputSize);
  sha512_final(&ctx, out);
}

void sha512_do(void* in, int inputSize, void* out) {
  SHA512_CTX ctx;
  sha512_init(&ctx);
  sha512_update(&ctx, in, inputSize);
  sha512_final(&ctx, out);
}
