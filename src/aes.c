﻿/**
 * BCS
 * BENNACEF Tahar
 * SOULATRE Manuel
 * 
 * AES implementation
 *
 * One AES context should not be used in two parallel threads!
 *
 */

#include "aes.h"

#define AES_COLUMNS 4
#define AES_COLSIZE 4
#define AES_BLOCK_SIZE 16

#define xtime(x) (((x) << 1u) ^ ((-((x) >= 0x80u)) & 0x1bu))

// Rijndael s-box
static const unsigned char sbox[256] = {
  0x63, 0x7c, 0x77, 0x7b, 0xf2, 0x6b, 0x6f, 0xc5, 0x30, 0x01, 0x67, 0x2b,
  0xfe, 0xd7, 0xab, 0x76, 0xca, 0x82, 0xc9, 0x7d, 0xfa, 0x59, 0x47, 0xf0,
  0xad, 0xd4, 0xa2, 0xaf, 0x9c, 0xa4, 0x72, 0xc0, 0xb7, 0xfd, 0x93, 0x26,
  0x36, 0x3f, 0xf7, 0xcc, 0x34, 0xa5, 0xe5, 0xf1, 0x71, 0xd8, 0x31, 0x15,
  0x04, 0xc7, 0x23, 0xc3, 0x18, 0x96, 0x05, 0x9a, 0x07, 0x12, 0x80, 0xe2,
  0xeb, 0x27, 0xb2, 0x75, 0x09, 0x83, 0x2c, 0x1a, 0x1b, 0x6e, 0x5a, 0xa0,
  0x52, 0x3b, 0xd6, 0xb3, 0x29, 0xe3, 0x2f, 0x84, 0x53, 0xd1, 0x00, 0xed,
  0x20, 0xfc, 0xb1, 0x5b, 0x6a, 0xcb, 0xbe, 0x39, 0x4a, 0x4c, 0x58, 0xcf,
  0xd0, 0xef, 0xaa, 0xfb, 0x43, 0x4d, 0x33, 0x85, 0x45, 0xf9, 0x02, 0x7f,
  0x50, 0x3c, 0x9f, 0xa8, 0x51, 0xa3, 0x40, 0x8f, 0x92, 0x9d, 0x38, 0xf5,
  0xbc, 0xb6, 0xda, 0x21, 0x10, 0xff, 0xf3, 0xd2, 0xcd, 0x0c, 0x13, 0xec,
  0x5f, 0x97, 0x44, 0x17, 0xc4, 0xa7, 0x7e, 0x3d, 0x64, 0x5d, 0x19, 0x73,
  0x60, 0x81, 0x4f, 0xdc, 0x22, 0x2a, 0x90, 0x88, 0x46, 0xee, 0xb8, 0x14,
  0xde, 0x5e, 0x0b, 0xdb, 0xe0, 0x32, 0x3a, 0x0a, 0x49, 0x06, 0x24, 0x5c,
  0xc2, 0xd3, 0xac, 0x62, 0x91, 0x95, 0xe4, 0x79, 0xe7, 0xc8, 0x37, 0x6d,
  0x8d, 0xd5, 0x4e, 0xa9, 0x6c, 0x56, 0xf4, 0xea, 0x65, 0x7a, 0xae, 0x08,
  0xba, 0x78, 0x25, 0x2e, 0x1c, 0xa6, 0xb4, 0xc6, 0xe8, 0xdd, 0x74, 0x1f,
  0x4b, 0xbd, 0x8b, 0x8a, 0x70, 0x3e, 0xb5, 0x66, 0x48, 0x03, 0xf6, 0x0e,
  0x61, 0x35, 0x57, 0xb9, 0x86, 0xc1, 0x1d, 0x9e, 0xe1, 0xf8, 0x98, 0x11,
  0x69, 0xd9, 0x8e, 0x94, 0x9b, 0x1e, 0x87, 0xe9, 0xce, 0x55, 0x28, 0xdf,
  0x8c, 0xa1, 0x89, 0x0d, 0xbf, 0xe6, 0x42, 0x68, 0x41, 0x99, 0x2d, 0x0f,
  0xb0, 0x54, 0xbb, 0x16
};

// reverse s-box
static const unsigned char rsbox[256] = {
  0x52, 0x09, 0x6a, 0xd5, 0x30, 0x36, 0xa5, 0x38, 0xbf, 0x40, 0xa3, 0x9e,
  0x81, 0xf3, 0xd7, 0xfb, 0x7c, 0xe3, 0x39, 0x82, 0x9b, 0x2f, 0xff, 0x87,
  0x34, 0x8e, 0x43, 0x44, 0xc4, 0xde, 0xe9, 0xcb, 0x54, 0x7b, 0x94, 0x32,
  0xa6, 0xc2, 0x23, 0x3d, 0xee, 0x4c, 0x95, 0x0b, 0x42, 0xfa, 0xc3, 0x4e,
  0x08, 0x2e, 0xa1, 0x66, 0x28, 0xd9, 0x24, 0xb2, 0x76, 0x5b, 0xa2, 0x49,
  0x6d, 0x8b, 0xd1, 0x25, 0x72, 0xf8, 0xf6, 0x64, 0x86, 0x68, 0x98, 0x16,
  0xd4, 0xa4, 0x5c, 0xcc, 0x5d, 0x65, 0xb6, 0x92, 0x6c, 0x70, 0x48, 0x50,
  0xfd, 0xed, 0xb9, 0xda, 0x5e, 0x15, 0x46, 0x57, 0xa7, 0x8d, 0x9d, 0x84,
  0x90, 0xd8, 0xab, 0x00, 0x8c, 0xbc, 0xd3, 0x0a, 0xf7, 0xe4, 0x58, 0x05,
  0xb8, 0xb3, 0x45, 0x06, 0xd0, 0x2c, 0x1e, 0x8f, 0xca, 0x3f, 0x0f, 0x02,
  0xc1, 0xaf, 0xbd, 0x03, 0x01, 0x13, 0x8a, 0x6b, 0x3a, 0x91, 0x11, 0x41,
  0x4f, 0x67, 0xdc, 0xea, 0x97, 0xf2, 0xcf, 0xce, 0xf0, 0xb4, 0xe6, 0x73,
  0x96, 0xac, 0x74, 0x22, 0xe7, 0xad, 0x35, 0x85, 0xe2, 0xf9, 0x37, 0xe8,
  0x1c, 0x75, 0xdf, 0x6e, 0x47, 0xf1, 0x1a, 0x71, 0x1d, 0x29, 0xc5, 0x89,
  0x6f, 0xb7, 0x62, 0x0e, 0xaa, 0x18, 0xbe, 0x1b, 0xfc, 0x56, 0x3e, 0x4b,
  0xc6, 0xd2, 0x79, 0x20, 0x9a, 0xdb, 0xc0, 0xfe, 0x78, 0xcd, 0x5a, 0xf4,
  0x1f, 0xdd, 0xa8, 0x33, 0x88, 0x07, 0xc7, 0x31, 0xb1, 0x12, 0x10, 0x59,
  0x27, 0x80, 0xec, 0x5f, 0x60, 0x51, 0x7f, 0xa9, 0x19, 0xb5, 0x4a, 0x0d,
  0x2d, 0xe5, 0x7a, 0x9f, 0x93, 0xc9, 0x9c, 0xef, 0xa0, 0xe0, 0x3b, 0x4d,
  0xae, 0x2a, 0xf5, 0xb0, 0xc8, 0xeb, 0xbb, 0x3c, 0x83, 0x53, 0x99, 0x61,
  0x17, 0x2b, 0x04, 0x7e, 0xba, 0x77, 0xd6, 0x26, 0xe1, 0x69, 0x14, 0x63,
  0x55, 0x21, 0x0c, 0x7d
};

static const unsigned char rcon[255] = {
  0x8d, 0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80, 0x1b, 0x36, 0x6c,
  0xd8, 0xab, 0x4d, 0x9a, 0x2f, 0x5e, 0xbc, 0x63, 0xc6, 0x97, 0x35, 0x6a,
  0xd4, 0xb3, 0x7d, 0xfa, 0xef, 0xc5, 0x91, 0x39, 0x72, 0xe4, 0xd3, 0xbd,
  0x61, 0xc2, 0x9f, 0x25, 0x4a, 0x94, 0x33, 0x66, 0xcc, 0x83, 0x1d, 0x3a,
  0x74, 0xe8, 0xcb, 0x8d, 0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80,
  0x1b, 0x36, 0x6c, 0xd8, 0xab, 0x4d, 0x9a, 0x2f, 0x5e, 0xbc, 0x63, 0xc6,
  0x97, 0x35, 0x6a, 0xd4, 0xb3, 0x7d, 0xfa, 0xef, 0xc5, 0x91, 0x39, 0x72,
  0xe4, 0xd3, 0xbd, 0x61, 0xc2, 0x9f, 0x25, 0x4a, 0x94, 0x33, 0x66, 0xcc,
  0x83, 0x1d, 0x3a, 0x74, 0xe8, 0xcb, 0x8d, 0x01, 0x02, 0x04, 0x08, 0x10,
  0x20, 0x40, 0x80, 0x1b, 0x36, 0x6c, 0xd8, 0xab, 0x4d, 0x9a, 0x2f, 0x5e,
  0xbc, 0x63, 0xc6, 0x97, 0x35, 0x6a, 0xd4, 0xb3, 0x7d, 0xfa, 0xef, 0xc5,
  0x91, 0x39, 0x72, 0xe4, 0xd3, 0xbd, 0x61, 0xc2, 0x9f, 0x25, 0x4a, 0x94,
  0x33, 0x66, 0xcc, 0x83, 0x1d, 0x3a, 0x74, 0xe8, 0xcb, 0x8d, 0x01, 0x02,
  0x04, 0x08, 0x10, 0x20, 0x40, 0x80, 0x1b, 0x36, 0x6c, 0xd8, 0xab, 0x4d,
  0x9a, 0x2f, 0x5e, 0xbc, 0x63, 0xc6, 0x97, 0x35, 0x6a, 0xd4, 0xb3, 0x7d,
  0xfa, 0xef, 0xc5, 0x91, 0x39, 0x72, 0xe4, 0xd3, 0xbd, 0x61, 0xc2, 0x9f,
  0x25, 0x4a, 0x94, 0x33, 0x66, 0xcc, 0x83, 0x1d, 0x3a, 0x74, 0xe8, 0xcb,
  0x8d, 0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80, 0x1b, 0x36, 0x6c,
  0xd8, 0xab, 0x4d, 0x9a, 0x2f, 0x5e, 0xbc, 0x63, 0xc6, 0x97, 0x35, 0x6a,
  0xd4, 0xb3, 0x7d, 0xfa, 0xef, 0xc5, 0x91, 0x39, 0x72, 0xe4, 0xd3, 0xbd,
  0x61, 0xc2, 0x9f, 0x25, 0x4a, 0x94, 0x33, 0x66, 0xcc, 0x83, 0x1d, 0x3a,
  0x74, 0xe8, 0xcb
};

void aesInit(AES_CTX* ctx, void* key, int keySize) {
  int i, j = 1;
  unsigned char* in = ctx->expandedKey;
  unsigned char* aesKey = key;
  ctx->aesRounds = (keySize / 4) + 6;

  for(i = 0 ; i < keySize ; i++) {
    in[i] = aesKey[i];
  }

  for( ; i < 16 * (ctx->aesRounds + 1) ; i+=4) {
    if(i % keySize == 0) {
      // apply s-box, and byte left rotation
      in[i+0] = sbox[in[i-3]] ^ in[i-keySize+0] ^ rcon[j++];
      in[i+1] = sbox[in[i-2]] ^ in[i-keySize+1];
      in[i+2] = sbox[in[i-1]] ^ in[i-keySize+2];
      in[i+3] = sbox[in[i-4]] ^ in[i-keySize+3];
    } else if(keySize == 32 && i % 32 == 16) {
      // extra s-box for 256 bit keys (no rotation / rcon)
      in[i+0] = sbox[in[i-4]] ^ in[i-32];
      in[i+1] = sbox[in[i-3]] ^ in[i-31];
      in[i+2] = sbox[in[i-2]] ^ in[i-30];
      in[i+3] = sbox[in[i-1]] ^ in[i-29];
    } else {
      in[i+0] = in[i-4] ^ in[i-keySize+0];
      in[i+1] = in[i-3] ^ in[i-keySize+1];
      in[i+2] = in[i-2] ^ in[i-keySize+2];
      in[i+3] = in[i-1] ^ in[i-keySize+3];
    }
  }
}

/* cipher a 128 bit block */
void aesCipher(AES_CTX* ctx, void* inData, void* outData) {
  int i,j,round;
  unsigned char* expandedKey = ctx->expandedKey;
  unsigned char* state = ctx->state;
  unsigned char* s = ctx->statealt;
  unsigned char* x = ctx->work;
  unsigned char* in = inData;
  unsigned char* out = outData;
  unsigned char aesRounds = ctx->aesRounds;

  // xor-ing first round key
  state[0*AES_COLSIZE+0] = in[0]  ^ expandedKey[0];
  state[0*AES_COLSIZE+1] = in[1]  ^ expandedKey[1];
  state[0*AES_COLSIZE+2] = in[2]  ^ expandedKey[2];
  state[0*AES_COLSIZE+3] = in[3]  ^ expandedKey[3];

  state[1*AES_COLSIZE+0] = in[4]  ^ expandedKey[4];
  state[1*AES_COLSIZE+1] = in[5]  ^ expandedKey[5];
  state[1*AES_COLSIZE+2] = in[6]  ^ expandedKey[6];
  state[1*AES_COLSIZE+3] = in[7]  ^ expandedKey[7];

  state[2*AES_COLSIZE+0] = in[8]  ^ expandedKey[8];
  state[2*AES_COLSIZE+1] = in[9]  ^ expandedKey[9];
  state[2*AES_COLSIZE+2] = in[10] ^ expandedKey[10];
  state[2*AES_COLSIZE+3] = in[11] ^ expandedKey[11];

  state[3*AES_COLSIZE+0] = in[12] ^ expandedKey[12];
  state[3*AES_COLSIZE+1] = in[13] ^ expandedKey[13];
  state[3*AES_COLSIZE+2] = in[14] ^ expandedKey[14];
  state[3*AES_COLSIZE+3] = in[15] ^ expandedKey[15];

  // rounds 1 to nr - 1
  for (round = 1; round < aesRounds; round++) {
    // sub-bytes && shift-rows
    s[0*AES_COLSIZE+0] = sbox[state[0*AES_COLSIZE+0]];
    s[0*AES_COLSIZE+1] = sbox[state[1*AES_COLSIZE+1]];
    s[0*AES_COLSIZE+2] = sbox[state[2*AES_COLSIZE+2]];
    s[0*AES_COLSIZE+3] = sbox[state[3*AES_COLSIZE+3]];

    s[1*AES_COLSIZE+0] = sbox[state[1*AES_COLSIZE+0]];
    s[1*AES_COLSIZE+1] = sbox[state[2*AES_COLSIZE+1]];
    s[1*AES_COLSIZE+2] = sbox[state[3*AES_COLSIZE+2]];
    s[1*AES_COLSIZE+3] = sbox[state[0*AES_COLSIZE+3]];

    s[2*AES_COLSIZE+0] = sbox[state[2*AES_COLSIZE+0]];
    s[2*AES_COLSIZE+1] = sbox[state[3*AES_COLSIZE+1]];
    s[2*AES_COLSIZE+2] = sbox[state[0*AES_COLSIZE+2]];
    s[2*AES_COLSIZE+3] = sbox[state[1*AES_COLSIZE+3]];

    s[3*AES_COLSIZE+0] = sbox[state[3*AES_COLSIZE+0]];
    s[3*AES_COLSIZE+1] = sbox[state[0*AES_COLSIZE+1]];
    s[3*AES_COLSIZE+2] = sbox[state[1*AES_COLSIZE+2]];
    s[3*AES_COLSIZE+3] = sbox[state[2*AES_COLSIZE+3]];

    for (i = 0; i < AES_COLUMNS; i++) {
      // mix-columns
      for (j = 0; j < AES_COLSIZE; j++) {
        x[j] = xtime(s[i*AES_COLSIZE+j]);
      }
      state[i*AES_COLSIZE+0] = x[0] ^ x[1] ^ s[i*AES_COLSIZE+1] ^ s[i*AES_COLSIZE+2] ^ s[i*AES_COLSIZE+3];
      state[i*AES_COLSIZE+1] = x[1] ^ x[2] ^ s[i*AES_COLSIZE+0] ^ s[i*AES_COLSIZE+2] ^ s[i*AES_COLSIZE+3];
      state[i*AES_COLSIZE+2] = x[2] ^ x[3] ^ s[i*AES_COLSIZE+0] ^ s[i*AES_COLSIZE+1] ^ s[i*AES_COLSIZE+3];
      state[i*AES_COLSIZE+3] = x[0] ^ x[3] ^ s[i*AES_COLSIZE+0] ^ s[i*AES_COLSIZE+1] ^ s[i*AES_COLSIZE+2];

      // xor-ing next round key
      state[i*AES_COLSIZE+0] ^= expandedKey[round*AES_BLOCK_SIZE+i*4+0];
      state[i*AES_COLSIZE+1] ^= expandedKey[round*AES_BLOCK_SIZE+i*4+1];
      state[i*AES_COLSIZE+2] ^= expandedKey[round*AES_BLOCK_SIZE+i*4+2];
      state[i*AES_COLSIZE+3] ^= expandedKey[round*AES_BLOCK_SIZE+i*4+3];
    }
  } // round

  // sub-bytes, mix rows, & last round key (no mixcolumns in last round)
  out[0]  = sbox[state[0*AES_COLSIZE+0]] ^ expandedKey[aesRounds*AES_BLOCK_SIZE+0];
  out[1]  = sbox[state[1*AES_COLSIZE+1]] ^ expandedKey[aesRounds*AES_BLOCK_SIZE+1];
  out[2]  = sbox[state[2*AES_COLSIZE+2]] ^ expandedKey[aesRounds*AES_BLOCK_SIZE+2];
  out[3]  = sbox[state[3*AES_COLSIZE+3]] ^ expandedKey[aesRounds*AES_BLOCK_SIZE+3];

  out[4]  = sbox[state[1*AES_COLSIZE+0]] ^ expandedKey[aesRounds*AES_BLOCK_SIZE+4];
  out[5]  = sbox[state[2*AES_COLSIZE+1]] ^ expandedKey[aesRounds*AES_BLOCK_SIZE+5];
  out[6]  = sbox[state[3*AES_COLSIZE+2]] ^ expandedKey[aesRounds*AES_BLOCK_SIZE+6];
  out[7]  = sbox[state[0*AES_COLSIZE+3]] ^ expandedKey[aesRounds*AES_BLOCK_SIZE+7];

  out[8]  = sbox[state[2*AES_COLSIZE+0]] ^ expandedKey[aesRounds*AES_BLOCK_SIZE+8];
  out[9]  = sbox[state[3*AES_COLSIZE+1]] ^ expandedKey[aesRounds*AES_BLOCK_SIZE+9];
  out[10] = sbox[state[0*AES_COLSIZE+2]] ^ expandedKey[aesRounds*AES_BLOCK_SIZE+10];
  out[11] = sbox[state[1*AES_COLSIZE+3]] ^ expandedKey[aesRounds*AES_BLOCK_SIZE+11];

  out[12] = sbox[state[3*AES_COLSIZE+0]] ^ expandedKey[aesRounds*AES_BLOCK_SIZE+12];
  out[13] = sbox[state[0*AES_COLSIZE+1]] ^ expandedKey[aesRounds*AES_BLOCK_SIZE+13];
  out[14] = sbox[state[1*AES_COLSIZE+2]] ^ expandedKey[aesRounds*AES_BLOCK_SIZE+14];
  out[15] = sbox[state[2*AES_COLSIZE+3]] ^ expandedKey[aesRounds*AES_BLOCK_SIZE+15];
}

/* decipher a 128 bit block */
void aesDecipher(AES_CTX* ctx, void* inData, void* outData) {
  int i,j,round;
  unsigned char* expandedKey = ctx->expandedKey;
  unsigned char* state = ctx->state;
  unsigned char* s = ctx->statealt;
  unsigned char* x = ctx->work;
  unsigned char* in = inData;
  unsigned char* out = outData;
  unsigned char aesRounds = ctx->aesRounds;

  // inverse sub-bytes, mix rows, & last round key (no mixcolumns in last round)
  state[0*AES_COLSIZE+0] = in[0]  ^ expandedKey[aesRounds*AES_BLOCK_SIZE+0];
  state[0*AES_COLSIZE+1] = in[1]  ^ expandedKey[aesRounds*AES_BLOCK_SIZE+1];
  state[0*AES_COLSIZE+2] = in[2]  ^ expandedKey[aesRounds*AES_BLOCK_SIZE+2];
  state[0*AES_COLSIZE+3] = in[3]  ^ expandedKey[aesRounds*AES_BLOCK_SIZE+3];

  state[1*AES_COLSIZE+0] = in[4]  ^ expandedKey[aesRounds*AES_BLOCK_SIZE+4];
  state[1*AES_COLSIZE+1] = in[5]  ^ expandedKey[aesRounds*AES_BLOCK_SIZE+5];
  state[1*AES_COLSIZE+2] = in[6]  ^ expandedKey[aesRounds*AES_BLOCK_SIZE+6];
  state[1*AES_COLSIZE+3] = in[7]  ^ expandedKey[aesRounds*AES_BLOCK_SIZE+7];

  state[2*AES_COLSIZE+0] = in[8]  ^ expandedKey[aesRounds*AES_BLOCK_SIZE+8];
  state[2*AES_COLSIZE+1] = in[9]  ^ expandedKey[aesRounds*AES_BLOCK_SIZE+9];
  state[2*AES_COLSIZE+2] = in[10] ^ expandedKey[aesRounds*AES_BLOCK_SIZE+10];
  state[2*AES_COLSIZE+3] = in[11] ^ expandedKey[aesRounds*AES_BLOCK_SIZE+11];

  state[3*AES_COLSIZE+0] = in[12] ^ expandedKey[aesRounds*AES_BLOCK_SIZE+12];
  state[3*AES_COLSIZE+1] = in[13] ^ expandedKey[aesRounds*AES_BLOCK_SIZE+13];
  state[3*AES_COLSIZE+2] = in[14] ^ expandedKey[aesRounds*AES_BLOCK_SIZE+14];
  state[3*AES_COLSIZE+3] = in[15] ^ expandedKey[aesRounds*AES_BLOCK_SIZE+15];

  // rounds 1 to nr - 1
  for(round = aesRounds-1 ; round > 0 ; round--) {
    // inverse shift-rows, sub-bytes & xor-ing round key
    s[0*AES_COLSIZE+0] = rsbox[state[0*AES_COLSIZE+0]] ^ expandedKey[round*AES_BLOCK_SIZE+0];
    s[0*AES_COLSIZE+1] = rsbox[state[3*AES_COLSIZE+1]] ^ expandedKey[round*AES_BLOCK_SIZE+1];
    s[0*AES_COLSIZE+2] = rsbox[state[2*AES_COLSIZE+2]] ^ expandedKey[round*AES_BLOCK_SIZE+2];
    s[0*AES_COLSIZE+3] = rsbox[state[1*AES_COLSIZE+3]] ^ expandedKey[round*AES_BLOCK_SIZE+3];

    s[1*AES_COLSIZE+0] = rsbox[state[1*AES_COLSIZE+0]] ^ expandedKey[round*AES_BLOCK_SIZE+4];
    s[1*AES_COLSIZE+1] = rsbox[state[0*AES_COLSIZE+1]] ^ expandedKey[round*AES_BLOCK_SIZE+5];
    s[1*AES_COLSIZE+2] = rsbox[state[3*AES_COLSIZE+2]] ^ expandedKey[round*AES_BLOCK_SIZE+6];
    s[1*AES_COLSIZE+3] = rsbox[state[2*AES_COLSIZE+3]] ^ expandedKey[round*AES_BLOCK_SIZE+7];

    s[2*AES_COLSIZE+0] = rsbox[state[2*AES_COLSIZE+0]] ^ expandedKey[round*AES_BLOCK_SIZE+8];
    s[2*AES_COLSIZE+1] = rsbox[state[1*AES_COLSIZE+1]] ^ expandedKey[round*AES_BLOCK_SIZE+9];
    s[2*AES_COLSIZE+2] = rsbox[state[0*AES_COLSIZE+2]] ^ expandedKey[round*AES_BLOCK_SIZE+10];
    s[2*AES_COLSIZE+3] = rsbox[state[3*AES_COLSIZE+3]] ^ expandedKey[round*AES_BLOCK_SIZE+11];

    s[3*AES_COLSIZE+0] = rsbox[state[3*AES_COLSIZE+0]] ^ expandedKey[round*AES_BLOCK_SIZE+12];
    s[3*AES_COLSIZE+1] = rsbox[state[2*AES_COLSIZE+1]] ^ expandedKey[round*AES_BLOCK_SIZE+13];
    s[3*AES_COLSIZE+2] = rsbox[state[1*AES_COLSIZE+2]] ^ expandedKey[round*AES_BLOCK_SIZE+14];
    s[3*AES_COLSIZE+3] = rsbox[state[0*AES_COLSIZE+3]] ^ expandedKey[round*AES_BLOCK_SIZE+15];

    // inverse mix-columns
    for (i=0; i < AES_COLUMNS; i++) {
      x[0] = s[i*AES_COLSIZE+0];
      x[1] = xtime(x[0]);
      x[2] = xtime(x[1]);
      x[3] = xtime(x[2]);

      state[i*AES_COLSIZE+0] = x[1] ^ x[2] ^ x[3];
      state[i*AES_COLSIZE+1] = x[0] ^ x[3];
      state[i*AES_COLSIZE+2] = x[0] ^ x[2] ^ x[3];
      state[i*AES_COLSIZE+3] = x[0] ^ x[1] ^ x[3];

      for (j = 1; j < AES_COLSIZE; j++) {
        x[0] = s[i*AES_COLSIZE+j];
        x[1] = xtime(x[0]);
        x[2] = xtime(x[1]);
        x[3] = xtime(x[2]);

        state[i*AES_COLSIZE+j]       ^= x[3] ^ x[2] ^ x[1];
        state[i*AES_COLSIZE+(j+1)%4] ^= x[3] ^ x[0];
        state[i*AES_COLSIZE+(j+2)%4] ^= x[3] ^ x[2] ^ x[0];
        state[i*AES_COLSIZE+j-1 ]    ^= x[3] ^ x[1] ^ x[0];
      }
    }
  } // round

  // inverse shift-rows, sub-bytes & xor roundkey
  out[0]  = rsbox[state[0*AES_COLSIZE+0]] ^ expandedKey[0];
  out[1]  = rsbox[state[3*AES_COLSIZE+1]] ^ expandedKey[1];
  out[2]  = rsbox[state[2*AES_COLSIZE+2]] ^ expandedKey[2];
  out[3]  = rsbox[state[1*AES_COLSIZE+3]] ^ expandedKey[3];

  out[4]  = rsbox[state[1*AES_COLSIZE+0]] ^ expandedKey[4];
  out[5]  = rsbox[state[0*AES_COLSIZE+1]] ^ expandedKey[5];
  out[6]  = rsbox[state[3*AES_COLSIZE+2]] ^ expandedKey[6];
  out[7]  = rsbox[state[2*AES_COLSIZE+3]] ^ expandedKey[7];

  out[8]  = rsbox[state[2*AES_COLSIZE+0]] ^ expandedKey[8];
  out[9]  = rsbox[state[1*AES_COLSIZE+1]] ^ expandedKey[9];
  out[10] = rsbox[state[0*AES_COLSIZE+2]] ^ expandedKey[10];
  out[11] = rsbox[state[3*AES_COLSIZE+3]] ^ expandedKey[11];

  out[12] = rsbox[state[3*AES_COLSIZE+0]] ^ expandedKey[12];
  out[13] = rsbox[state[2*AES_COLSIZE+1]] ^ expandedKey[13];
  out[14] = rsbox[state[1*AES_COLSIZE+2]] ^ expandedKey[14];
  out[15] = rsbox[state[0*AES_COLSIZE+3]] ^ expandedKey[15];
}

void aesClean(AES_CTX* ctx) {
  unsigned int i;
  volatile unsigned char* ctxPtr = (unsigned char*)ctx;
  for(i=0; i < sizeof(AES_CTX); i++) {
    ctxPtr[i] = 0;
  }
}
