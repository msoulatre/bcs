/**
 * BCS
 * SOULATRE Manuel
 * 
 * HMAC implementation, works with any hash function
 *
 */

#ifndef H_HMAC
#define H_HMAC

#include "sha2.h"

#define MAX_CHUNK_SIZE SHA2_CHUNK_SIZE_512

typedef void (*initFunc)(void*);
typedef void (*updateFunc)(void*, void*, int);
typedef void (*finalFunc)(void*, void*);

struct HMAC_CTX_STRUCT {
  unsigned char chunkBuffer[MAX_CHUNK_SIZE];
  unsigned char expandedKey[MAX_CHUNK_SIZE];
  void* hashCtx;
  int hashChunkSize;
  int hashOutputSize;
  initFunc hashInit;
  updateFunc hashUpdate;
  finalFunc hashFinal;
};

typedef struct HMAC_CTX_STRUCT HMAC_CTX;

void hmac_init_sha224(HMAC_CTX* ctx, SHA256_CTX* hashCtx, void* hmacKey, int hmacKeySize);
void hmac_init_sha256(HMAC_CTX* ctx, SHA256_CTX* hashCtx, void* hmacKey, int hmacKeySize);
void hmac_init_sha384(HMAC_CTX* ctx, SHA512_CTX* hashCtx, void* hmacKey, int hmacKeySize);
void hmac_init_sha512(HMAC_CTX* ctx, SHA512_CTX* hashCtx, void* hmacKey, int hmacKeySize);

void hmac_update(HMAC_CTX* ctx, void* in, int inputSize);
void hmac_final(HMAC_CTX* ctx, void* out);

#endif
