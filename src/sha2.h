/**
 * BCS
 * SOULATRE Manuel
 * 
 * SHA-2 implementation
 *
 */

#ifndef H_SHA2
#define H_SHA2

#include <stdint.h>

#define SHA2_CHUNK_SIZE_256 64
#define SHA2_CHUNK_SIZE_512 128

#define SHA2_OUTPUT_SIZE_224 28
#define SHA2_OUTPUT_SIZE_256 32
#define SHA2_OUTPUT_SIZE_384 48
#define SHA2_OUTPUT_SIZE_512 64

struct SHA256_CTX_S {
  uint32_t state[8];
  uint64_t totalLength;
  uint8_t  outputSize;
  uint8_t  buffer[SHA2_CHUNK_SIZE_256];
  uint_fast8_t bufferLength;
};

struct SHA512_CTX_S {
  uint64_t state[8];
  uint64_t totalLength[2];
  uint8_t  outputSize;
  uint8_t  buffer[SHA2_CHUNK_SIZE_512];
  uint_fast8_t bufferLength;
};

typedef struct SHA256_CTX_S SHA256_CTX;
typedef struct SHA512_CTX_S SHA512_CTX;

void sha224_init(SHA256_CTX* ctx);
void sha256_init(SHA256_CTX* ctx);
void sha384_init(SHA512_CTX* ctx);
void sha512_init(SHA512_CTX* ctx);
void sha256_update(SHA256_CTX* ctx, void* in, int inputSize);
void sha512_update(SHA512_CTX* ctx, void* in, int inputSize);
void sha256_final(SHA256_CTX* ctx, void* out);
void sha512_final(SHA512_CTX* ctx, void* out);
void sha224_do(void* in, int inputSize, void* out);
void sha256_do(void* in, int inputSize, void* out);
void sha384_do(void* in, int inputSize, void* out);
void sha512_do(void* in, int inputSize, void* out);

#endif
