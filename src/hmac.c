/**
 * BCS
 * SOULATRE Manuel
 * 
 * HMAC implementation, works with any hash function
 *
 */

#include "hmac.h"

#define IPAD 0x36
#define OPAD 0x5C

void hmac_init(HMAC_CTX* ctx, void* hmacKey, int hmacKeySize) {
  unsigned char* hKey = hmacKey;
  int i;
  ctx->hashInit(ctx->hashCtx);

  // shorten or enlarge key
  if(hmacKeySize > ctx->hashChunkSize) {
    // key is too big, hash it
    ctx->hashUpdate(ctx->hashCtx, hmacKey, hmacKeySize);
    ctx->hashFinal(ctx->hashCtx, ctx->expandedKey);
    hmacKeySize = ctx->hashOutputSize;
  } else {
    // key is ok or smaller than hash chunk, use it as is
    for(i=0; i < hmacKeySize; i++) {
      ctx->expandedKey[i] = hKey[i];
    }
  }
  // pad key
  for(i=hmacKeySize; i < ctx->hashChunkSize; i++) {
    ctx->expandedKey[i] = 0;
  }

  // begin the main hash with the ipaded key
  for(i=0; i < ctx->hashChunkSize; i++) {
    ctx->chunkBuffer[i] = ctx->expandedKey[i] ^ IPAD;
  }
  ctx->hashUpdate(ctx->hashCtx, ctx->chunkBuffer, ctx->hashChunkSize);
}

void hmac_update(HMAC_CTX* ctx, void* in, int inputSize) {
  ctx->hashUpdate(ctx->hashCtx, in, inputSize);
}

void hmac_final(HMAC_CTX* ctx, void* out) {
  int i;
  // compute first hash
  ctx->hashFinal(ctx->hashCtx, out);

  // hash first hash xored with opaded key
  for(i=0; i < ctx->hashChunkSize; i++) {
    ctx->chunkBuffer[i] = ctx->expandedKey[i] ^ OPAD;
  }
  ctx->hashUpdate(ctx->hashCtx, ctx->chunkBuffer, ctx->hashChunkSize);
  ctx->hashUpdate(ctx->hashCtx, out, ctx->hashOutputSize);
  ctx->hashFinal(ctx->hashCtx, out);
}

void hmac_init_sha224(HMAC_CTX* ctx, SHA256_CTX* hashCtx, void* hmacKey, int hmacKeySize) {
  ctx->hashCtx = hashCtx;
  ctx->hashChunkSize = SHA2_CHUNK_SIZE_256;
  ctx->hashOutputSize = SHA2_OUTPUT_SIZE_224;
  ctx->hashInit = (initFunc)sha224_init;
  ctx->hashUpdate = (updateFunc)sha256_update;
  ctx->hashFinal = (finalFunc)sha256_final;
  hmac_init(ctx, hmacKey, hmacKeySize);
}

void hmac_init_sha256(HMAC_CTX* ctx, SHA256_CTX* hashCtx, void* hmacKey, int hmacKeySize) {
  ctx->hashCtx = hashCtx;
  ctx->hashChunkSize = SHA2_CHUNK_SIZE_256;
  ctx->hashOutputSize = SHA2_OUTPUT_SIZE_256;
  ctx->hashInit = (initFunc)sha256_init;
  ctx->hashUpdate = (updateFunc)sha256_update;
  ctx->hashFinal = (finalFunc)sha256_final;
  hmac_init(ctx, hmacKey, hmacKeySize);
}

void hmac_init_sha384(HMAC_CTX* ctx, SHA512_CTX* hashCtx, void* hmacKey, int hmacKeySize) {
  ctx->hashCtx = hashCtx;
  ctx->hashChunkSize = SHA2_CHUNK_SIZE_512;
  ctx->hashOutputSize = SHA2_OUTPUT_SIZE_384;
  ctx->hashInit = (initFunc)sha384_init;
  ctx->hashUpdate = (updateFunc)sha512_update;
  ctx->hashFinal = (finalFunc)sha512_final;
  hmac_init(ctx, hmacKey, hmacKeySize);
}

void hmac_init_sha512(HMAC_CTX* ctx, SHA512_CTX* hashCtx, void* hmacKey, int hmacKeySize) {
  ctx->hashCtx = hashCtx;
  ctx->hashChunkSize = SHA2_CHUNK_SIZE_512;
  ctx->hashOutputSize = SHA2_OUTPUT_SIZE_512;
  ctx->hashInit = (initFunc)sha512_init;
  ctx->hashUpdate = (updateFunc)sha512_update;
  ctx->hashFinal = (finalFunc)sha512_final;
  hmac_init(ctx, hmacKey, hmacKeySize);
}

/*
// one go hash independent implementation
void hmac_any(HMAC_CTX* ctx, void* hmacKey, int hmacKeySize, void* in, int inputSize, void* out) {
  uint8_t* hKey = hmacKey;
  int i;

  ctx->hashInit(ctx->hashCtx);

  // shorten or enlarge key
  if(hmacKeySize > ctx->hashChunkSize) {
    // key is too big, hash it
    ctx->hashUpdate(ctx->hashCtx, hmacKey, hmacKeySize);
    ctx->hashFinal(ctx->hashCtx, ctx->expandedKey);
    hmacKeySize = ctx->hashOutputSize;
  } else {
    // key is ok or smaller than hash chunk, use it as is
    for(i=0; i < hmacKeySize; i++) {
      ctx->expandedKey[i] = hKey[i];
    }
  }
  // pad key
  for(i=hmacKeySize; i < ctx->hashChunkSize; i++) {
    ctx->expandedKey[i] = 0;
  }

  // main hmac algorithm
  for(i=0; i < ctx->hashChunkSize; i++) {
    ctx->chunkBuffer[i] = ctx->expandedKey[i] ^ IPAD;
  }
  ctx->hashUpdate(ctx->hashCtx, ctx->chunkBuffer, ctx->hashChunkSize);
  ctx->hashUpdate(ctx->hashCtx, in, inputSize);
  ctx->hashFinal(ctx->hashCtx, out);
  
  for(i=0; i < ctx->hashChunkSize; i++) {
    ctx->chunkBuffer[i] = ctx->expandedKey[i] ^ OPAD;
  }
  ctx->hashUpdate(ctx->hashCtx, ctx->chunkBuffer, ctx->hashChunkSize);
  ctx->hashUpdate(ctx->hashCtx, out, ctx->hashOutputSize);
  ctx->hashFinal(ctx->hashCtx, out);
}
*/
