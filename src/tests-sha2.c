#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "sha2.h"
#include "tests-utils.h"

#define SHA224_SIZE 224
#define SHA256_SIZE 256
#define SHA384_SIZE 384
#define SHA512_SIZE 512

int rspTest(FILE* fileIn, long outputSize) {
  char fileBuffer[513];
  unsigned char startData[256];
  unsigned char expected[64];
  unsigned char output[64];
  SHA256_CTX context256;
  SHA512_CTX context512;

  switch(outputSize) {
    case SHA224_SIZE:
      sha224_init(&context256);
      break;
    case SHA256_SIZE:
      sha256_init(&context256);
      break;
    case SHA384_SIZE:
      sha384_init(&context512);
      break;
    case SHA512_SIZE:
      sha512_init(&context512);
      break;
    default:
      puts("Invalid output size!");
      return -1;
  }

  int i = 0, errors = 0;
  int inputSize1, inputSize2;

  long inputSize;

  while(fgets(fileBuffer, sizeof(fileBuffer), fileIn) != NULL) {
    if(buffcmp(fileBuffer, "Len", 3) == 0) {
      inputSize = strtol(fileBuffer + 6, NULL, 10);
      if(fgets(fileBuffer, 7, fileIn) == NULL || fileBuffer[1] != 's') {
        puts("IO fail !");
        return -1;
      }
      int len;
      
      // input
      do {
        if(fgets(fileBuffer, sizeof(fileBuffer), fileIn) == NULL) {
          puts("IO fail !");
          return -1;
        }
        len = strlen(fileBuffer);
        inputSize1 = inputSize == 0 ? 0 : (len/ 2) - (fileBuffer[len - 1] == '\n' ? 1 : 0);
        toHex(startData, fileBuffer, inputSize1);
        switch(outputSize) {
        case SHA224_SIZE:
        case SHA256_SIZE:
          sha256_update(&context256, startData, inputSize1);
          break;
        case SHA384_SIZE:
        case SHA512_SIZE:
          sha512_update(&context512, startData, inputSize1);
          break;
        }
      } while(fileBuffer[len - 1] != '\n');

      // expected output
      if(fgets(fileBuffer, sizeof(fileBuffer), fileIn) == NULL || fileBuffer[1] != 'D') {
        puts("IO fail !");
        return -1;
      }
      inputSize2 = (strlen(fileBuffer + 5) - 1) / 2;
      toHex(expected, fileBuffer + 5, inputSize2);
      i++;
        switch(outputSize) {
        case SHA224_SIZE:
        case SHA256_SIZE:
          sha256_final(&context256, output);
          break;
        case SHA384_SIZE:
        case SHA512_SIZE:
          sha512_final(&context512, output);
          break;
        }
      if(memcmp(expected, output, outputSize/8) == 0) {
        printf(".");
      } else {
        printf("x");
        errors++;
      }
    }
  }

  puts("");
  printf("\nRan %d tests, with %d failures!\n", i, errors);

  return 0;
}

int main(int argc, char* argv[]) {
  FILE * fileIn = NULL;
  char fileName[128];
  long outputSize;

  if(argc > 2) {
    outputSize = strtol(argv[1], NULL, 10);
    fileIn = fopen(argv[2], "r");
    strncpy(fileName, argv[2], 127);
  } else {
    puts("Program expects an output size and file as an input!");
    return -1;
  }

  if(fileIn == NULL) {
    puts("IO error!");
    return -1;
  } else {
    printf("\n== SHA2 input test file %s ==\n", fileName);
    return rspTest(fileIn, outputSize);
  }
}
