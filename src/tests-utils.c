#include <stdio.h>
#include "tests-utils.h"

/** buffcmp
 * compares two buffers for buffSize chars
 * returns 0 if the buffers are equal 
 * else, return -1 */
int buffcmp(void* first, void* second, int buffSize) {
  int i;
  char* buff1 = first;
  char* buff2 = second;
  for(i=0 ; buff1[i] == buff2[i] && i < buffSize ; i++);
  
  return (i == buffSize) ? 0 : -1;
}

void printh(void* input, int size, int columns) {
  unsigned char* hex = input;
  int i;
  for(i=0; i < size; i++) {
    if(i != 0 && columns != 0 && (i % columns) == 0) {
      puts("");
    }
    printf("%02x ", *(hex+i));
  }
  puts("");
}

unsigned char asciitohex(unsigned char C1, unsigned char C2)
{
  unsigned char ret = 0;
  if((C1 >= '0') && (C1 <= '9')) {
    ret = (C1 - '0') << 4;
  } else if((C1 >= 'A') && (C1 <= 'F')) {
    ret = (C1 - 'A'+10) << 4;
  } if((C1 >= 'a') && (C1 <= 'f')) {
    ret = (C1 - 'a'+10) << 4;
  }
  
  if((C2 >= '0') && (C2 <= '9')) {
    ret+= C2 - '0';
  } if((C2 >= 'A') && (C2 <= 'F')) {
    ret+= C2 - 'A'+10;
  } if((C2 >= 'a') && (C2 <= 'f')) {
    ret+= C2 - 'a'+10;
  }
  return ret;
}


void toHex(void* out, void* in, int size) {
  unsigned char* ascii = in;
  unsigned char* hex = out;
  int i;
  
  for(i = 0 ; i < size ; i++) {
    hex[i] = asciitohex(ascii[i*2], ascii[i*2+1]);
  }
}
