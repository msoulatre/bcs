/**
 * BCS
 * BENNACEF Tahar
 * SOULATRE VAREA Manuel
 * 
 * MD4 implementation
 *
 */

#ifndef H_MD4
#define H_MD4

void md4Message(unsigned char * in, unsigned char out[16], int size);

#endif
