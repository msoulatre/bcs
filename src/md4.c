/**
 * BCS - TP
 * BENNACEF Tahar
 * SOULATRE VAREA Manuel
 * M2 SSI
 * 
 * Primitive de cryptage hach MD4
 *
 */
 
#include<stdio.h>
#include<stdint.h>
#include<string.h>
#include<stdlib.h>

#ifdef _MSC_VER
#define INLINEFUNC __inline
#else
#define INLINEFUNC inline
#endif

#define RAC2 0x5A827999
#define RAC3 0x6ED9EBA1

INLINEFUNC uint32_t if32(uint32_t x, uint32_t y, uint32_t z) {
  return ( (x & y) | (~x & z) ) ;
}

INLINEFUNC uint32_t maj32(uint32_t x, uint32_t y, uint32_t z) {
  return ( (x & y) | (x & z) | (y & z) )  ;
}

INLINEFUNC uint32_t xor332(uint32_t x, uint32_t y, uint32_t z) {
  return (x ^ y ^ z);
}

INLINEFUNC uint32_t rot32l(uint32_t x, int r /*val rotation*/) {
  uint32_t msb = x << r;
  uint32_t lsb = x >> (32 - r); 
  return (msb | lsb);
}


INLINEFUNC uint32_t round1(uint32_t in0, uint32_t in1, uint32_t in2, uint32_t in3, uint32_t mess, int8_t rotfl /*val rotation*/) {
  return rot32l( (in0 + if32(in1, in2, in3) + mess), rotfl);
}


INLINEFUNC uint32_t round2(uint32_t in0, uint32_t in1, uint32_t in2, uint32_t in3, uint32_t mess, int8_t rotfl /*val rotation*/) {
  return rot32l( (in0 + maj32(in1, in2, in3) + mess + RAC2), rotfl);
}

INLINEFUNC uint32_t round3(uint32_t in0, uint32_t in1, uint32_t in2, uint32_t in3, uint32_t mess, int8_t rotfl /*val rotation*/) {
  return rot32l( (in0 + xor332(in1, in2, in3) + mess + RAC3), rotfl);
}


void CMD4(uint32_t chv[4], uint32_t mess[16]) {
/**
 * 2014-11-02 MSV : passe a deux arguments, le resultat ecrase l'ancien contenu de chv
 * 
 */
  
  // INIT
  uint32_t out[4] = { chv[0], chv[1], chv[2], chv[3] };
  
  /* Round 1 */
  out[0] = round1(out[0], out[1], out[2], out[3], mess[0], 3);
  out[3] = round1(out[3], out[0], out[1], out[2], mess[1], 7);
  out[2] = round1(out[2], out[3], out[0], out[1], mess[2], 11);
  out[1] = round1(out[1], out[2], out[3], out[0], mess[3], 19);
  
  out[0] = round1(out[0], out[1], out[2], out[3], mess[4], 3);
  out[3] = round1(out[3], out[0], out[1], out[2], mess[5], 7);
  out[2] = round1(out[2], out[3], out[0], out[1], mess[6], 11);
  out[1] = round1(out[1], out[2], out[3], out[0], mess[7], 19);
  
  out[0] = round1(out[0], out[1], out[2], out[3], mess[8], 3);
  out[3] = round1(out[3], out[0], out[1], out[2], mess[9], 7);
  out[2] = round1(out[2], out[3], out[0], out[1], mess[10], 11);
  out[1] = round1(out[1], out[2], out[3], out[0], mess[11], 19);
  
  out[0] = round1(out[0], out[1], out[2], out[3], mess[12], 3);
  out[3] = round1(out[3], out[0], out[1], out[2], mess[13], 7);
  out[2] = round1(out[2], out[3], out[0], out[1], mess[14], 11);
  out[1] = round1(out[1], out[2], out[3], out[0], mess[15], 19);

  
  /* Round 2 */
  out[0] = round2(out[0], out[1], out[2], out[3], mess[0], 3);
  out[3] = round2(out[3], out[0], out[1], out[2], mess[4], 5);
  out[2] = round2(out[2], out[3], out[0], out[1], mess[8], 9);
  out[1] = round2(out[1], out[2], out[3], out[0], mess[12], 13);
  
  out[0] = round2(out[0], out[1], out[2], out[3], mess[1], 3);
  out[3] = round2(out[3], out[0], out[1], out[2], mess[5], 5);
  out[2] = round2(out[2], out[3], out[0], out[1], mess[9], 9);
  out[1] = round2(out[1], out[2], out[3], out[0], mess[13], 13);
  
  out[0] = round2(out[0], out[1], out[2], out[3], mess[2], 3);
  out[3] = round2(out[3], out[0], out[1], out[2], mess[6], 5);
  out[2] = round2(out[2], out[3], out[0], out[1], mess[10], 9);
  out[1] = round2(out[1], out[2], out[3], out[0], mess[14], 13);
  
  out[0] = round2(out[0], out[1], out[2], out[3], mess[3], 3);
  out[3] = round2(out[3], out[0], out[1], out[2], mess[7], 5);
  out[2] = round2(out[2], out[3], out[0], out[1], mess[11], 9);
  out[1] = round2(out[1], out[2], out[3], out[0], mess[15], 13);
 
  /* Round 3 */
  out[0] = round3(out[0], out[1], out[2], out[3], mess[0], 3);
  out[3] = round3(out[3], out[0], out[1], out[2], mess[8], 9);
  out[2] = round3(out[2], out[3], out[0], out[1], mess[4], 11);
  out[1] = round3(out[1], out[2], out[3], out[0], mess[12], 15);
  
  out[0] = round3(out[0], out[1], out[2], out[3], mess[2], 3);
  out[3] = round3(out[3], out[0], out[1], out[2], mess[10], 9);
  out[2] = round3(out[2], out[3], out[0], out[1], mess[6], 11);
  out[1] = round3(out[1], out[2], out[3], out[0], mess[14], 15);


  out[0] = round3(out[0], out[1], out[2], out[3], mess[1], 3);
  out[3] = round3(out[3], out[0], out[1], out[2], mess[9], 9);
  out[2] = round3(out[2], out[3], out[0], out[1], mess[5], 11);
  out[1] = round3(out[1], out[2], out[3], out[0], mess[13], 15);


  out[0] = round3(out[0], out[1], out[2], out[3], mess[3], 3);
  out[3] = round3(out[3], out[0], out[1], out[2], mess[11], 9);
  out[2] = round3(out[2], out[3], out[0], out[1], mess[7], 11);
  out[1] = round3(out[1], out[2], out[3], out[0], mess[15], 15);
  
  chv[0] += out[0];
  chv[1] += out[1];
  chv[2] += out[2];
  chv[3] += out[3];
}

void md4Message(unsigned char * in, unsigned char out[16], int size) {
  unsigned char i;
  uint32_t paddingBuffer[16] = {0};
  uint32_t * buffer = (uint32_t *) out;
  uint32_t counter[2] = {0, 0}; // 64 bits as bit counter
  uint32_t counterNewValue;

  buffer[0] = 0x67452301;
  buffer[1] = 0xEFCDAB89;
  buffer[2] = 0x98BADCFE;
  buffer[3] = 0x10325476;
  
  /* md4 on all but last message (if not already congruent) */
  for(i = 0 ; i < size - (size%64) ; i += 64){
    CMD4(buffer, (uint32_t*) (in + i));
    // overflow detection
    counterNewValue = counter[0] + 64 * 8;
    if(counter[0] > counterNewValue) {
      counter[1]++;
    }
    counter[0] = counterNewValue;
  }
  
  // padding with (1000 0000)b
  memcpy(paddingBuffer, (in + i), size % 64);
  paddingBuffer[(size%64) / 4] |= 0x80 << 8 * ((size%64) % 4);
  
  // overflow detection
  counterNewValue = counter[0] + 64 * 8;
  if(counter[0] > counterNewValue) {
    counter[1]++;
  }
  counter[0] = counterNewValue;
  
  paddingBuffer[14] = counter[0];
  paddingBuffer[15] = counter[1];
  
  CMD4(buffer, paddingBuffer);
}
