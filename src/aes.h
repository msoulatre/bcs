﻿/**
 * BCS - TP
 * BENNACEF Tahar
 * SOULATRE Manuel
 * M2 SSI
 * 
 * AES implementation
 *
 */

#ifndef H_AES
#define H_AES

struct AES_CTX_S {
  unsigned char expandedKey[240];
  unsigned char state[4*4];
  unsigned char statealt[4*4];
  unsigned char work[4];
  unsigned char aesRounds;
};

typedef struct AES_CTX_S AES_CTX;

void aesInit(AES_CTX* ctx, void* aesKey, int keySize);
void aesCipher(AES_CTX* ctx, void* inData, void* outData);
void aesDecipher(AES_CTX* ctx, void* inData, void* outData);
void aesClean(AES_CTX* ctx);

#endif
