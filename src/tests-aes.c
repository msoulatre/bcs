#include <stdio.h>
#include <string.h>
#include "tests-utils.h"
#include "aes.h"

#define MODE_ENCRYPT 0
#define MODE_DECRYPT 1

void fipsTests() {
  unsigned char buffer[16];
  /**
   * key expansion example as given by fips-197 (A.3)
   * see http://csrc.nist.gov/publications/fips/fips197/fips-197.pdf
   */
  {
    unsigned char aesKey[32]    = {0x60, 0x3d, 0xeb, 0x10, 0x15, 0xca, 0x71, 0xbe, 0x2b, 0x73, 0xae, 0xf0, 0x85, 0x7d, 0x77, 0x81,
                                   0x1f, 0x35, 0x2c, 0x07, 0x3b, 0x61, 0x08, 0xd7, 0x2d, 0x98, 0x10, 0xa3, 0x09, 0x14, 0xdf, 0xf4};
    unsigned char expected[240] = {
      0x60, 0x3d, 0xeb, 0x10, 0x15, 0xca, 0x71, 0xbe, 0x2b, 0x73, 0xae, 0xf0, 0x85, 0x7d, 0x77, 0x81,
      0x1f, 0x35, 0x2c, 0x07, 0x3b, 0x61, 0x08, 0xd7, 0x2d, 0x98, 0x10, 0xa3, 0x09, 0x14, 0xdf, 0xf4,
      0x9b, 0xa3, 0x54, 0x11, 0x8e, 0x69, 0x25, 0xaf, 0xa5, 0x1a, 0x8b, 0x5f, 0x20, 0x67, 0xfc, 0xde,
      0xa8, 0xb0, 0x9c, 0x1a, 0x93, 0xd1, 0x94, 0xcd, 0xbe, 0x49, 0x84, 0x6e, 0xb7, 0x5d, 0x5b, 0x9a,
      0xd5, 0x9a, 0xec, 0xb8, 0x5b, 0xf3, 0xc9, 0x17, 0xfe, 0xe9, 0x42, 0x48, 0xde, 0x8e, 0xbe, 0x96,
      0xb5, 0xa9, 0x32, 0x8a, 0x26, 0x78, 0xa6, 0x47, 0x98, 0x31, 0x22, 0x29, 0x2f, 0x6c, 0x79, 0xb3,
      0x81, 0x2c, 0x81, 0xad, 0xda, 0xdf, 0x48, 0xba, 0x24, 0x36, 0x0a, 0xf2, 0xfa, 0xb8, 0xb4, 0x64,
      0x98, 0xc5, 0xbf, 0xc9, 0xbe, 0xbd, 0x19, 0x8e, 0x26, 0x8c, 0x3b, 0xa7, 0x09, 0xe0, 0x42, 0x14,
      0x68, 0x00, 0x7b, 0xac, 0xb2, 0xdf, 0x33, 0x16, 0x96, 0xe9, 0x39, 0xe4, 0x6c, 0x51, 0x8d, 0x80,
      0xc8, 0x14, 0xe2, 0x04, 0x76, 0xa9, 0xfb, 0x8a, 0x50, 0x25, 0xc0, 0x2d, 0x59, 0xc5, 0x82, 0x39,
      0xde, 0x13, 0x69, 0x67, 0x6c, 0xcc, 0x5a, 0x71, 0xfa, 0x25, 0x63, 0x95, 0x96, 0x74, 0xee, 0x15,
      0x58, 0x86, 0xca, 0x5d, 0x2e, 0x2f, 0x31, 0xd7, 0x7e, 0x0a, 0xf1, 0xfa, 0x27, 0xcf, 0x73, 0xc3,
      0x74, 0x9c, 0x47, 0xab, 0x18, 0x50, 0x1d, 0xda, 0xe2, 0x75, 0x7e, 0x4f, 0x74, 0x01, 0x90, 0x5a,
      0xca, 0xfa, 0xaa, 0xe3, 0xe4, 0xd5, 0x9b, 0x34, 0x9a, 0xdf, 0x6a, 0xce, 0xbd, 0x10, 0x19, 0x0d,
      0xfe, 0x48, 0x90, 0xd1, 0xe6, 0x18, 0x8d, 0x0b, 0x04, 0x6d, 0xf3, 0x44, 0x70, 0x6c, 0x63, 0x1e
    };
    
    puts("\n== AES 256-bit key expansion test ==");
    
    AES_CTX context;
    aesInit(&context, aesKey, 32);
    
    puts("Key: ");
    printh(aesKey, sizeof(aesKey), 16);
    puts("\nExpansion:");
    printh(context.expandedKey, sizeof(context.expandedKey), 16);
    
    if(buffcmp(context.expandedKey, expected, sizeof(expected)) != 0) {
      puts("\nError! Showing expected result:");
      printh(expected, sizeof(expected), 16);
    } else {
      puts("\nTest passed!");
    }
    aesClean(&context);
  }

  /**
   * cipher and decipher example as given by fips-197 (C.3)
   * see http://csrc.nist.gov/publications/fips/fips197/fips-197.pdf
   */
  {
    unsigned char aesKey[32]    = {0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f,
                                   0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x1a, 0x1b, 0x1c, 0x1d, 0x1e, 0x1f};
    unsigned char plainText[16] = {0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xaa, 0xbb, 0xcc, 0xdd, 0xee, 0xff};
    unsigned char expected[16]  = {0x8e, 0xa2, 0xb7, 0xca, 0x51, 0x67, 0x45, 0xbf, 0xea, 0xfc, 0x49, 0x90, 0x4b, 0x49, 0x60, 0x89};
    
    puts("\n== AES 256-bit key cipher test ==");
    
    AES_CTX context;
    aesInit(&context, aesKey, 32);
    aesCipher(&context, plainText, buffer);
    
    puts("Key: ");
    printh(aesKey, sizeof(aesKey), 16);
    puts("\nClear text:");
    printh(plainText, sizeof(plainText), 16);
    puts("\nCiphered text:");
    printh(buffer, sizeof(buffer), 16);
    
    if(buffcmp(buffer, expected, sizeof(expected)) != 0) {
      puts("\nError! Showing expected result:");
      printh(expected, sizeof(expected), 16);
    } else {
      puts("\nTest passed!");
    }
    
    aesDecipher(&context, buffer, buffer);
    
    puts("\n== AES 256-bit key decipher test ==");
    
    puts("Deciphered text:");
    printh(buffer, sizeof(buffer), 16);
    
    if(buffcmp(buffer, plainText, sizeof(plainText)) != 0) {
      puts("\nError! Showing expected result:");
      printh(plainText, sizeof(plainText), 16);
    } else {
      puts("\nTest passed!");
    }
    aesClean(&context);
  }
}

void xorer(void* data1, void* data2, int n) {
  int i;
  for(i = 0 ; i < n ; i++) {
    *(((char*) data1) + i) = *(((char*) data1) + i) ^ *(((char*) data2) + i);
  }
}

int rspTest(FILE* fileIn, int iterations) {
  char fileBuffer[128];
  unsigned char aesKey[32];
  unsigned char expected[16];
  unsigned char startData[16];
  unsigned char cipherBuffer[16];
  unsigned char invertBuffer[16];
  int keySize = 128;
  AES_CTX context;
  char mode = MODE_ENCRYPT;
  
  int i = 0, j, cipherErrors = 0, decipherErrors = 0;
  
  while(fgets(fileBuffer, sizeof(fileBuffer), fileIn) != NULL) {
    if(buffcmp(fileBuffer, "[ENCRYPT]", 9) == 0) {
      mode = MODE_ENCRYPT;
    } else if(buffcmp(fileBuffer, "[DECRYPT]", 9) == 0) {
      mode = MODE_DECRYPT;
    } else if(buffcmp(fileBuffer, "COUNT", 5) == 0) {
      int args = 0;
      while(args != 3) {
        if(fgets(fileBuffer, sizeof(fileBuffer), fileIn) == NULL) {
          puts("IO fail !");
          return -1;
        }
        
        switch(fileBuffer[0]) {
          case 'K': // KEY
            keySize = (strnlen(fileBuffer + 6, 65) - 1) / 2;
            toHex(aesKey, fileBuffer + 6, keySize);
            args++;
            break;
          case 'P': // PLAINTEXT
            if(mode == MODE_ENCRYPT) {
              toHex(startData, fileBuffer + 12, sizeof(startData));
            } else {
              toHex(expected, fileBuffer + 12, sizeof(expected));
            }
            args++;
            break;
          case 'C': // CIPHERTEXT
            if(mode == MODE_ENCRYPT) {
              toHex(expected, fileBuffer + 13, sizeof(expected));
            } else {
              toHex(startData, fileBuffer + 13, sizeof(startData));
            }
            args++;
            break;
        }
      }
      
      
      // has everything it needs to test
      i++;
      aesInit(&context, aesKey, keySize);
      memcpy(cipherBuffer, startData, sizeof(startData));
      if(mode == MODE_ENCRYPT) {
        for(j = 0; j < iterations ; j++) {
          aesCipher(&context, cipherBuffer, cipherBuffer);
        }
        // running inverse test
        memcpy(invertBuffer, cipherBuffer, sizeof(invertBuffer));
        for(j = 0; j < iterations ; j++) {
          aesDecipher(&context, invertBuffer, invertBuffer);
        }
      } else {
        for(j = 0; j < iterations ; j++) {
          aesDecipher(&context, cipherBuffer, cipherBuffer);
        }
        // running inverse test
        memcpy(invertBuffer, cipherBuffer, sizeof(invertBuffer));
        for(j = 0; j < iterations ; j++) {
          aesCipher(&context, invertBuffer, invertBuffer);
        }
      }

      if(buffcmp(cipherBuffer, expected, sizeof(expected)) == 0) {
        if(buffcmp(startData, invertBuffer, sizeof(startData)) == 0) {
          printf(".");
        } else {
          printf("~");
          if(mode == MODE_ENCRYPT) {
            decipherErrors++;
          } else {
            cipherErrors++;
          }
        }
      } else {
        printf("x");
        if(mode == MODE_ENCRYPT) {
          cipherErrors++;
        } else {
          decipherErrors++;
        }
      }
      
      aesClean(&context);
    }
  }
  
  printf("\nRan %d tests, with %d cipher failures and %d decipher failures !\n", i, cipherErrors, decipherErrors);
  
  return 0;
}

int main(int argc, char* argv[]) {
  int iterations = 1;
  FILE * fileIn = NULL;
  char fileName[128];

  if(argc == 1) {
    fipsTests();
    return 0;
  }

  if(argc > 2) {
    if(argv[1][0] == '-' && (argv[1][1] == 'm' || (argv[1][2] == 'm'))) {
      iterations = 1000;
    }
    fileIn  = fopen(argv[2], "r");
    strncpy(fileName, argv[2], 127);
  } else {
    fileIn  = fopen(argv[1], "r");
    strncpy(fileName, argv[1], 127);
  }

  if(fileIn == NULL) {
    puts("IO error!");
    return -1;
  } else {
    printf("\n== AES input test file %s ==\n", fileName);
    return rspTest(fileIn, iterations);
  }
}
