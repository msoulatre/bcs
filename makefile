CC=gcc
CFLAGS=-W -Wall
BINFOLDER=./bin
SRCFOLDER=./src
OBJFOLDER=./obj
TESTFOLDER=./tests
OPTLEVEL=-O3

all: $(BINFOLDER)/bcs

$(BINFOLDER)/bcs: $(OBJFOLDER)/aes.o $(OBJFOLDER)/bcs.o  $(OBJFOLDER)/sha2.o
	$(CC) -o $(BINFOLDER)/bcs $(OBJFOLDER)/bcs.o $(OBJFOLDER)/aes.o $(OBJFOLDER)/sha2.o $(OPTLEVEL) $(CFLAGS)

$(OBJFOLDER)/bcs.o: $(SRCFOLDER)/bcs.c $(SRCFOLDER)/aes.h $(SRCFOLDER)/sha2.h
	$(CC) -o $(OBJFOLDER)/bcs.o -c $(SRCFOLDER)/bcs.c $(OPTLEVEL) $(CFLAGS)

$(OBJFOLDER)/aes.o: $(SRCFOLDER)/aes.c $(SRCFOLDER)/aes.h
	$(CC) -o $(OBJFOLDER)/aes.o -c $(SRCFOLDER)/aes.c $(OPTLEVEL) $(CFLAGS)

$(OBJFOLDER)/sha2.o: $(SRCFOLDER)/sha2.c
	$(CC) -o $(OBJFOLDER)/sha2.o -c $(SRCFOLDER)/sha2.c $(OPTLEVEL) $(CFLAGS)

$(OBJFOLDER)/md4.o: $(SRCFOLDER)/md4.c
	$(CC) -o $(OBJFOLDER)/md4.o -c $(SRCFOLDER)/md4.c $(OPTLEVEL) $(CFLAGS)

$(OBJFOLDER)/hmac.o: $(SRCFOLDER)/hmac.c $(SRCFOLDER)/hmac.h
	$(CC) -o $(OBJFOLDER)/hmac.o -c $(SRCFOLDER)/hmac.c $(OPTLEVEL) $(CFLAGS)

$(OBJFOLDER)/tests-aes.o: $(SRCFOLDER)/tests-aes.c $(SRCFOLDER)/aes.h $(SRCFOLDER)/tests-utils.h
	$(CC) -o $(OBJFOLDER)/tests-aes.o -c $(SRCFOLDER)/tests-aes.c $(OPTLEVEL) $(CFLAGS)

$(OBJFOLDER)/tests-sha2.o: $(SRCFOLDER)/tests-sha2.c $(SRCFOLDER)/sha2.h $(SRCFOLDER)/tests-utils.h
	$(CC) -o $(OBJFOLDER)/tests-sha2.o -c $(SRCFOLDER)/tests-sha2.c $(OPTLEVEL) $(CFLAGS)

$(OBJFOLDER)/tests-hmac.o: $(SRCFOLDER)/tests-hmac.c $(SRCFOLDER)/hmac.h $(SRCFOLDER)/tests-utils.h
	$(CC) -o $(OBJFOLDER)/tests-hmac.o -c $(SRCFOLDER)/tests-hmac.c $(OPTLEVEL) $(CFLAGS)

$(OBJFOLDER)/tests-utils.o: $(SRCFOLDER)/tests-utils.c
	$(CC) -o $(OBJFOLDER)/tests-utils.o -c $(SRCFOLDER)/tests-utils.c $(OPTLEVEL) $(CFLAGS)

aes-test:$(OBJFOLDER)/tests-aes.o $(OBJFOLDER)/aes.o $(OBJFOLDER)/tests-utils.o NISTFILES-AES FORCE
	@$(CC) -o $(BINFOLDER)/tests-aes $(OBJFOLDER)/tests-aes.o $(OBJFOLDER)/tests-utils.o $(OBJFOLDER)/aes.o $(OPTLEVEL) $(CFLAGS)
	@$(BINFOLDER)/tests-aes
	@$(BINFOLDER)/tests-aes -m $(TESTFOLDER)/ECBMCT128.rsp
	@$(BINFOLDER)/tests-aes $(TESTFOLDER)/ECBKeySbox128.rsp
	@$(BINFOLDER)/tests-aes $(TESTFOLDER)/ECBGFSbox128.rsp
	@$(BINFOLDER)/tests-aes $(TESTFOLDER)/ECBVarKey128.rsp
	@$(BINFOLDER)/tests-aes $(TESTFOLDER)/ECBVarTxt128.rsp
	@$(BINFOLDER)/tests-aes -m $(TESTFOLDER)/ECBMCT192.rsp
	@$(BINFOLDER)/tests-aes $(TESTFOLDER)/ECBKeySbox192.rsp
	@$(BINFOLDER)/tests-aes $(TESTFOLDER)/ECBGFSbox192.rsp
	@$(BINFOLDER)/tests-aes $(TESTFOLDER)/ECBVarKey192.rsp
	@$(BINFOLDER)/tests-aes $(TESTFOLDER)/ECBVarTxt192.rsp
	@$(BINFOLDER)/tests-aes -m $(TESTFOLDER)/ECBMCT256.rsp
	@$(BINFOLDER)/tests-aes $(TESTFOLDER)/ECBKeySbox256.rsp
	@$(BINFOLDER)/tests-aes $(TESTFOLDER)/ECBGFSbox256.rsp
	@$(BINFOLDER)/tests-aes $(TESTFOLDER)/ECBVarKey256.rsp
	@$(BINFOLDER)/tests-aes $(TESTFOLDER)/ECBVarTxt256.rsp
	@rm $(BINFOLDER)/tests-aes

sha-test:$(OBJFOLDER)/tests-sha2.o $(OBJFOLDER)/sha2.o $(OBJFOLDER)/tests-utils.o NISTFILES-SHA FORCE
	@$(CC) -o $(BINFOLDER)/tests-sha2 $(OBJFOLDER)/tests-sha2.o $(OBJFOLDER)/tests-utils.o $(OBJFOLDER)/sha2.o $(OPTLEVEL) $(CFLAGS)
	@$(BINFOLDER)/tests-sha2 224 $(TESTFOLDER)/SHA224ShortMsg.rsp
	@$(BINFOLDER)/tests-sha2 224 $(TESTFOLDER)/SHA224LongMsg.rsp
	@$(BINFOLDER)/tests-sha2 256 $(TESTFOLDER)/SHA256ShortMsg.rsp
	@$(BINFOLDER)/tests-sha2 256 $(TESTFOLDER)/SHA256LongMsg.rsp
	@$(BINFOLDER)/tests-sha2 384 $(TESTFOLDER)/SHA384ShortMsg.rsp
	@$(BINFOLDER)/tests-sha2 384 $(TESTFOLDER)/SHA384LongMsg.rsp
	@$(BINFOLDER)/tests-sha2 512 $(TESTFOLDER)/SHA512ShortMsg.rsp
	@$(BINFOLDER)/tests-sha2 512 $(TESTFOLDER)/SHA512LongMsg.rsp
	@rm $(BINFOLDER)/tests-sha2

hmac-test:$(OBJFOLDER)/tests-hmac.o $(OBJFOLDER)/hmac.o $(OBJFOLDER)/sha2.o $(OBJFOLDER)/tests-utils.o FORCE
	@$(CC) -o $(BINFOLDER)/tests-hmac $(OBJFOLDER)/tests-hmac.o $(OBJFOLDER)/tests-utils.o $(OBJFOLDER)/sha2.o $(OBJFOLDER)/hmac.o $(OPTLEVEL) $(CFLAGS)
	@$(BINFOLDER)/tests-hmac
	@rm $(BINFOLDER)/tests-hmac

test: aes-test sha-test hmac-test

runtest: $(BINFOLDER)/bcs
	@echo "Generating a random file..."
	@dd if=/dev/urandom of=$(TESTFOLDER)/file bs=10M count=1 status=none
	./bin/bcs -c $(TESTFOLDER)/file     -o $(TESTFOLDER)/ciphered -k test
	./bin/bcs -d $(TESTFOLDER)/ciphered -o $(TESTFOLDER)/clear    -k test
	@cmp $(TESTFOLDER)/file $(TESTFOLDER)/clear
	@if cmp $(TESTFOLDER)/file $(TESTFOLDER)/clear ; then\
		echo "Success, files are identical!";\
	fi
	@echo "Cleaning up..."
	@rm $(TESTFOLDER)/file $(TESTFOLDER)/clear $(TESTFOLDER)/ciphered -f

NISTFILES-AES: $(TESTFOLDER)/ECBMCT256.rsp $(TESTFOLDER)/ECBKeySbox256.rsp $(TESTFOLDER)/ECBGFSbox256.rsp $(TESTFOLDER)/ECBVarKey256.rsp $(TESTFOLDER)/ECBVarTxt256.rsp $(TESTFOLDER)/ECBMCT192.rsp $(TESTFOLDER)/ECBKeySbox192.rsp $(TESTFOLDER)/ECBGFSbox192.rsp $(TESTFOLDER)/ECBVarKey192.rsp $(TESTFOLDER)/ECBVarTxt192.rsp $(TESTFOLDER)/ECBMCT128.rsp $(TESTFOLDER)/ECBKeySbox128.rsp $(TESTFOLDER)/ECBGFSbox128.rsp $(TESTFOLDER)/ECBVarKey128.rsp $(TESTFOLDER)/ECBVarTxt128.rsp 
NISTFILES-SHA: $(TESTFOLDER)/SHA224ShortMsg.rsp $(TESTFOLDER)/SHA224LongMsg.rsp $(TESTFOLDER)/SHA256ShortMsg.rsp $(TESTFOLDER)/SHA256LongMsg.rsp $(TESTFOLDER)/SHA384ShortMsg.rsp $(TESTFOLDER)/SHA384LongMsg.rsp $(TESTFOLDER)/SHA512ShortMsg.rsp $(TESTFOLDER)/SHA512LongMsg.rsp

FORCE:

clean:
	rm -rf $(OBJFOLDER)/*


